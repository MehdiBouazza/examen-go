package main

import (
	"io"
	"log"
	"net/http"
)

type Task struct {
	Description string
	Done        bool
}
var task []Task


	func list(rw http.ResponseWriter, _ *http.Request){
		io.WriteString(w, "list!\n")
	}
	func done(w http.ResponseWriter, _ *http.Request) {
		io.WriteString(w, "done!\n")
	}
	func add(w http.ResponseWriter, _ *http.Request) {
		io.WriteString(w, "add!\n")
	}
	
func main() {
	http.HandleFunc("/", list)
	http.HandleFunc("/done", done)
	http.HandleFunc("/add", add)

	log.Fatal(http.ListenAndServe(":8080", nil))
}
